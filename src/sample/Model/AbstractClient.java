package sample.Model;

public abstract class AbstractClient extends Thread {
    String response;

    public String getResponse() {
        return response;
    }

    public abstract void performRequest(String request);
    abstract void receiveResponse();
}
