package sample.Model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client extends AbstractClient {
    private final String serverAddress;
    private final Integer port;

    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    public Client(String serverAddress, Integer port) {
        this.serverAddress = serverAddress;
        this.port = port;
    }

    public String getResponse() {
        return response;
    }

    @Override
    public void run() {
        try {
            socket = new Socket(serverAddress, port);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            //e.printStackTrace();
            System.err.println("Cannot initialize i/o streams.");
        }
    }

    @Override
    public void performRequest(String request) {
        sendRequest(request);
        receiveResponse();
    }

    private void sendRequest(String request) {
        out.print(request + "\r\n");
        out.flush();
    }

    @Override
    void receiveResponse() {
        try {
            int c;
            StringBuilder response = new StringBuilder();

            while ((c = in.read()) != '\n') {
                response.append((char) c);
            }
            this.response = response.toString();
        } catch (IOException e) {
            //e.printStackTrace();
            System.err.println("Error while getting response from server.");
        }
    }
}
