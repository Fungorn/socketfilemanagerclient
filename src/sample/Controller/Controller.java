package sample.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import sample.Model.AbstractClient;
import sample.Model.Client;

import java.util.StringTokenizer;

public class Controller {

    private AbstractClient client;
    private StringBuilder responseBuilder = new StringBuilder();
    private ObservableList<String> list;
    private ListView<String> listView;

    @FXML
    private StackPane stackPane;

    @FXML
    private TextFlow textFlow;

    private String[] getResponseData() {
        responseBuilder = new StringBuilder();
        StringTokenizer st = new StringTokenizer(client.getResponse(), "[]/, ");
        responseBuilder.append(". ");
        responseBuilder.append(".. ");
        while(st.hasMoreTokens()) {
            responseBuilder.append(st.nextToken());
            responseBuilder.append(" ");
        }
        return responseBuilder.toString().split(" ");
    }

    @FXML
    void initialize() {
        client = new Client("127.0.0.1", 8005);
        //client.start();
        client.run();
        client.performRequest("/");


        list = FXCollections.observableArrayList(getResponseData());
        listView = new ListView<>(list);

        listView.setCellFactory(param -> new FileCell());
        stackPane.getChildren().add(listView);
    }

    private class FileCell extends ListCell<String> { // Element of left-side list of folders/files
        private HBox hbox = new HBox();
        private Hyperlink link = new Hyperlink("(empty)");
        private Pane pane = new Pane();
        private String lastItem;

        FileCell() {
            super();
            hbox.getChildren().addAll(link, pane);
            HBox.setHgrow(pane, Priority.ALWAYS);
        }

        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            setText(null);
            if (empty) {
                lastItem = null;
                setGraphic(null);
            } else {
                lastItem = item;
                link.setText(item != null ? ('/' + item) : "<null>");
                link.setOnAction(actionEvent -> { // On element click listener
                    client.performRequest(item); // Navigate to /<item>-folder
                    if (item != null && item.contains(".txt")) { // Show content of simple text file
                        textFlow.getChildren().clear();
                        Text fileTextData = new Text(client.getResponse());
                        textFlow.getChildren().add(fileTextData);
                    } else {
                        list.removeAll(responseBuilder.toString().split(" ")); // Refresh list contents
                        list.addAll(getResponseData());
                    }
                });
                setGraphic(hbox);
            }
        }
    }
}
